<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Marketplaces extends CI_Controller{

  function __construct(){
    parent::__construct();
    $this->load->helper('security');
    $this->load->library('Aauth');
    $this->load->library('form_validation');
    $this->load->library('Datatables');
    $this->load->model('Marketplaces_model');
    $this->load->model('Group_model');
  }

  /**
     *
     Marketplaces index
     * */
     
  function index(){
    $data['admin_data'] = $this->checkAccess();
    $data['permissions'] = getUserPermissions();
    $data['title'] = 'Marketplaces';
    $this->load->view('include/inner_header',$data);
    $this->load->view('marketplaces/marketplaces_listing',$data);
    $this->load->view('include/inner_footer',$data);

  }

  /*
  * data for marketplace datatable
  * @return json
  */
  function marketplaces_listing(){
    $_POST['sColumns'] = '';
    $data["admin_data"] = $this->checkAccess();
    $logged_in_user_id=$this->aauth->get_user_id();
    $this->datatables->select('marketplaces.marketplace_name,marketplaces.marketplace_id,marketplaces.created')
    ->where('marketplaces.is_deleted',0)
    ->from('marketplaces');
    echo $this->datatables->generate();
  }

  /**
  * Add marketplace
  * @return json
  */

  public function add_marketplace(){

    $data["admin_data"] = $this->checkAccess();

    $this->form_validation->set_rules('marketplace_name','Marketplace Name','trim|required|xss_clean|is_unique[marketplaces.marketplace_name]');

    if($this->form_validation->run()){
      if($this->Marketplaces_model->add_marketplace()){
        $result["status"] = 1;
        $result["message"] = $this->lang->line('marketplace_added_success');
      }else{
        $result["status"] = 0;
        $result["message"] =  $this->lang->line('Error_occured');
      }
    }else{
      $result["status"] = 0;
      $result["message"] = validation_errors();
    }
    echo json_encode($result);
  }

  /**
  * generate edit marketplace form
  * @return void
  */
  function edit_marketplace($id){

    $data["admin_data"] = $this->checkAccess();

    $data["details"] = $this->Marketplaces_model->get_marketplace_details($id);

    $this->load->view('marketplaces/edit_marketplace',$data);
  }

  /**
  * Edit marketplace
  * @return json
  */
  function update_marketplace($id){

    $data['admin_data'] = $this->checkAccess();

    $unique = checkEditUnique($id,$this->input->post('marketplace_name'),'marketplaces','marketplace_name','marketplace_id');

    if($unique){
      $this->form_validation->set_rules('marketplace_name','Marketplace Name','trim|required|xss_clean');

      if($this->form_validation->run()){
        if($this->Marketplaces_model->update_marketplace($id)){
          $result["status"] = 1;
          $result["message"] = $this->lang->line('marketplace_update_success');
        }else{
          $result["status"] = 0;
          $result["message"] =  $this->lang->line('Error_occured');
        }
      }else{
        $result["status"] = 0;
        $result["message"] = validation_errors();
      }
    }else {
      $result["status"] = 0;
      $result["message"] = $this->lang->line('unique_marketplace_name_error');
    }

    echo json_encode($result);
  }

  /**
  * Delete marketplace
  * @return json
  */

  public function delete_marketplace($id){

    $data['admin_data'] = $this->checkAccess();
    $del = $this->Marketplaces_model->delete($id);

    if($del){
      $result['message'] = $this->lang->line('marketplace_delete_success');
      $result['status'] = 1;
    }else {
      $result['message'] = $this->lang->line('error_occured');
      $result['status'] = 0;
    }

    echo json_encode($result);
  }

  /** Verify if user is logged in and is admin (for direct requests)
  * @param void
  * @return mixed
  */

  private function checkAccess(){

    if($this->aauth->is_loggedin()){
      return  $this->session->all_userdata();
    }
    else{
      redirect('/auth/login/');
    }
  }
}
?>
