<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Purchases_return extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('security');
        $this->load->library('Datatables');
        $this->load->library('Aauth');
        $this->load->library('form_validation');
        $this->load->model('Purchase_return_model');
    }
    

    /**
     * 
     Purchases index
     * */

     public function index()
     {
        $data["admin_data"] = $this->checkAccess();
        $data['permissions'] = getUserPermissions();
        $data["title"] = "Purchase Returns";
        $this->load->view('include/inner_header', $data);
        $this->load->view('purchases_return/purchase_return_listing', $data);
        $this->load->view('include/inner_footer', $data);
    }
    
    /**
    * Load purchase_return_listing  for datatable
    *
    * @return json
    */
    public function purchase_return_listing()
    {
        $_POST['sColumns'] = '';
        $data["admin_data"] = $this->checkAccess();   
        $looged_in_user_id=$this->aauth->get_user_id();
        $this->datatables->select('vendors.vendor_name,purchases.purchase_date,SUM(
            purchase_items.total_cost),purchases.created,purchases.purchase_id')
        ->where('purchases.is_returned',1)
        ->from('purchases')
        ->join('vendors','purchases.vendor_id=vendors.vendor_id','left')
         ->join('purchase_items','purchase_items.purchase_id = purchases.purchase_id')
        ->group_by('purchases.purchase_id');
        echo $this->datatables->generate();
    }
    
  /**
     * 
     new_purchase 
     * */

     public function return_purchase()
     {
        $data["admin_data"] = $this->checkAccess();
        $data['permissions'] = getUserPermissions();
        $data["title"] = "Purchase Return";
        $data["vendors"] = $this->Purchase_return_model->get_vendors();
        $data["marketplaces"] = $this->Purchase_return_model->get_marketplaces();
        $data["categories"] = $this->Purchase_return_model->get_categories();
        $this->load->view('include/inner_header', $data);
        $this->load->view('purchases_return/return_purchase', $data);
        $this->load->view('include/inner_footer', $data);
    }
   /**
     * save_purchase_return
     * */

       public function save_purchase_return(){
        $data["admin_data"] = $this->checkAccess();
        $this->form_validation->set_rules('vendor_name','Vendor Name','trim|required|xss_clean');
        $this->form_validation->set_rules('marketplace_name','Marketplace Name','trim|required|xss_clean');
        $this->form_validation->set_rules('date_purchased','Date Purchased','trim|required|xss_clean');
        $this->form_validation->set_rules('date_listed','Date listed','trim|xss_clean');
            if($this->form_validation->run()){
              if($stock=$this->Purchase_return_model->save_purchase_return()){
                if(is_array($stock)){
                 $result["status"] = 0;
                  $result["message"] =  'Quantity available in stock for '.$stock['0']['item_title'].' is <strong>'.$stock['0']['stock'].'</strong>.<br />Please reduce purchase return quantity.';
               }else{
                $result["status"] = 1;
                $result["message"] = $this->lang->line('purchase_return_successfully_added');
               }
              }else{
                $result["status"] = 0;
                $result["message"] =  $this->lang->line('purchase_stock_error');
              }
            }else{
              $result["status"] = 0;
              $result["message"] = validation_errors();
            }
        echo json_encode($result);
     }
     
    /**
     * view_purchase_return
     * */

      public function view_purchase_return($purchase_id)
       {
        $data["admin_data"] = $this->checkAccess();
        $data['permissions'] = getUserPermissions();
        $data["title"] = "View Purchase Return";
        $data["purchase_id"] = $purchase_id;
        $data["details"] = $this->Purchase_return_model->get_purchase($purchase_id);
        if(!$data["details"])
        redirect('Purchases');
        
        $this->load->view('include/inner_header', $data);
        $this->load->view('purchases_return/view_purchase_return', $data);
        $this->load->view('include/inner_footer', $data);
    }

     /**
     * purchase_items_listing()
     * */
    public function purchase_return_items_listing()
    {
        $data["admin_data"] = $this->checkAccess();
        $this->datatables->select('purchase_items.purchase_item_id,purchase_items.item_title,purchase_items.sku,categories.category_name,purchase_items.location,purchase_items.date_listed,purchase_items.conditions,purchase_items.item_qty,purchase_items.unit_cost,purchase_items.estimate_price_per_unit,purchase_items.total_cost')
        ->from('purchase_items');
        $this->datatables->join('categories','purchase_items.category_id = categories.category_id','left');
        $this->datatables->where('purchase_id',$this->input->post('purchase_id'));
        echo $this->datatables->generate();
        
    }
    
     /**
     * edit_purchase()
     * */
    public function edit_purchase_return($purchase_id)
    {
        $data["admin_data"] = $this->checkAccess();
        $data['user_id'] = $this->aauth->get_user_id();
        $data["purchase_id"] = $purchase_id;
        $data["vendors"] = $this->Purchase_return_model->get_vendors();
        $data["marketplaces"] = $this->Purchase_return_model->get_marketplaces();
        $data["details"] = $this->Purchase_return_model->get_purchase_details($purchase_id);   
        $this->load->view('purchases_return/edit_purchase_return', $data);
    }
 
     /**
     *  update_purchase_return
     * */
     public function update_purchase_return($purchase_id) 
     {
        $data["admin_data"] = $this->checkAccess();
        $this->form_validation->set_rules('vendor_name','Vendor Name','trim|required|xss_clean');
        $this->form_validation->set_rules('marketplace_name','Marketplace Name','trim|required|xss_clean');
        $this->form_validation->set_rules('date_purchased','Date Purchased','trim|required|xss_clean');
        $this->form_validation->set_rules('date_listed','Date listed','trim|xss_clean');       
         if($this->form_validation->run())
            {  
               if ($this->Purchase_return_model->update_purchase_return($purchase_id))
                    {
                        $result["status"] = 1;
                        $result["message"] =$this->lang->line('purchase_return_update_success');
                    } else {
                        $result["status"] = 0;
                        $result["message"] = $this->lang->line('Error_occured');
                    }
            }
           else
           {
                    $result["status"] = 0;
                    $result["message"] =  validation_errors();
           }
          echo json_encode($result);
    }

       /**
     *  edit_purchase_return_items
     * */

       public function edit_purchase_return_item($purchase_id)
       {
        $data["admin_data"] = $this->checkAccess();
        $data['user_id'] = $this->aauth->get_user_id();
        $data["categories"] = $this->Purchase_return_model->get_categories();
        $data["details"] = $this->Purchase_return_model->get_purchase_item($purchase_id);
        $this->load->view('purchases_return/edit_purchase_return_item',$data);
       }

        /**
         *update_purchase_return_item
          * */

       public function update_purchase_return_item($purchase_item_id) 
        {
        $data["admin_data"] = $this->checkAccess();
        $this->form_validation->set_rules('category_name', 'Category Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('item_title', 'Title', 'trim|required|xss_clean');
        $this->form_validation->set_rules('item_qty', 'Quantity', 'trim|required|xss_clean');
        $this->form_validation->set_rules('unit_cost', 'Unit Cost', 'trim|required|xss_clean');
        $this->form_validation->set_rules('total_cost', 'Total cost', 'trim|required|xss_clean');           
         if($this->form_validation->run())
            {  
               if ($resulti=$this->Purchase_return_model->update_purchase_return_item($purchase_item_id))
                    {
                        if(is_array($resulti)){
                             $result["status"] = 0;
                              $result["message"] =  'Quantity available in stock for '.$resulti['item_title'].' is <strong>'.$resulti['stock'].'</strong>.<br />Please reduce purchase return quantity.';
                           }
                        else
                        {
                        $result["status"] = 1;
                        $result["message"] = $this->lang->line('purchase_return_item_update_success');
                        }   
                    } 
                    else {
                        $result["status"] = 0;
                        $result["message"] = $this->lang->line('Error_occured');
                    }
            }
           else
           {
                
                    $result["status"] = 0;
                    $result["message"] =  validation_errors();
           }

          echo json_encode($result);
    }

     /**
     *  delete_purchase_return_item
     * */

     public function delete_purchase_return_item($purchase_item_id)
     {
        $data["admin_data"] = $this->checkAccess();
        
        if($res =$this->Purchase_return_model->delete_purchase_return_item($purchase_item_id)){
           $this->_show_message('Purchase return item has been successfully deleted.','success','purchases/view_purchase/'.$res);
        }
        else{
           $this->_show_message('Error deleting record. Please try again later','error','purchases/view_purchase/'.$res);
        }  

        echo json_encode($result);
    }

    function _show_message($message,$css_class="info",$redirect='/auth/')
    {
        $this->session->set_flashdata('message', $message);
        $this->session->set_flashdata('class', $css_class);
        redirect($redirect);
    }   
    
/**
     *  search_products
     * */  
    public function search_product(){
        $data["admin_data"] = $this->checkAccess();
        if (isset($_GET['term'])) {
          $result = $this->Purchase_return_model->search_product($_GET['term']);
          if (count($result) > 0) {
            foreach ($result as $row)
              $arr_result[] = array(
                'label'         => $row->item_title,
                'product_id'   => $row->product_id,
              );
            echo json_encode($arr_result);
          }
        }
      }
/**
     *  get_products
     * */  
  public function get_product($product_id)
      {
        $items = $this->Purchase_return_model->get_products_details($product_id);
        echo json_encode($items);
      }     
/** Verify if user is logged in and is admin (for direct requests)
   * @param void
   * @return mixed
   */
private function checkAccess()
{  
 if($this->aauth->is_loggedin())
 {
    return  $this->session->all_userdata();
}
else
{
    redirect('auth'); 
    die();
}
}


}
?>
