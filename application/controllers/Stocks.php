<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Stocks extends CI_Controller{

  function __construct(){
    parent::__construct();
    $this->load->helper('security');
    $this->load->library('Aauth');
    $this->load->library('form_validation');
    $this->load->library('Datatables');
  }


  
/**
 * Stocks::index()
 * @return void
 */

  function index(){
    $data['admin_data'] = $this->checkAccess();
    $data['permissions'] = getUserPermissions();
    $data["title"] = "Stocks";
    $this->load->view('include/inner_header',$data);
    $this->load->view('stocks/stocks_listing',$data);
    $this->load->view('include/inner_footer',$data);

  }

  /*
  * data for category datatable
  * @return json
  */
  function stocks_listig(){
    $data["admin_data"] = $this->checkAccess();
    $logged_in_user_id=$this->aauth->get_user_id();
    $company_id=$this->session->userdata('company_id');
    $this->datatables->select('wherehouses.wherehouse_name,products.item_title,stocks.stock,stocks.stock_id')
    ->join('wherehouses','wherehouses.wherehouse_id = stocks.wherehouse_id')
    ->join('products','products.product_id = stocks.product_id')
    //->where('stocks.is_deleted',0)
    ->where('stocks.company_id',$company_id)
    ->from('stocks');
    echo $this->datatables->generate();
  }

  /** Verify if user is logged in and is admin (for direct requests)
  * @param void
  * @return mixed
  */

  private function checkAccess(){

    if($this->aauth->is_loggedin()){
      return  $this->session->all_userdata();
    }
    else{
      redirect('/auth/login/');
    }
  }
}
?>
