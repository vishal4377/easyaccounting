<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller
{   
  
    
	function __construct()
	{
		parent::__construct();

	    $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('security');
        $this->load->library('Aauth');
        $this->load->library('form_validation');
         $this->load->model('Dashboard_model');
	}

 public function index() {
            $data["admin_data"] = $this->checkAccess();
            $data['user_id'] = $this->aauth->get_user_id();
            $data['permissions'] = getUserPermissions();
            
            $data["title"] = "Dashboard";
            $this->load->view('include/inner_header', $data);
            $this->load->view('dashboard', $data);
            $this->load->view('include/inner_footer', $data);
    }
    
    public function get_data(){
        $data['total_products'] = $this->Dashboard_model->get_products();
        $data['total_payble_amount'] = $this->Dashboard_model->total_payble_amount();
        $data['total_outstanding'] = $this->Dashboard_model->total_outstanding();
        $data['total_clients'] = $this->Dashboard_model->total_clients();
        $data['sales_outstanding_amount'] = $this->Dashboard_model->sales_outstanding_amount();
        $data['purchase_payable_amount'] = $this->Dashboard_model->purchase_payable_amount();
        // echo "<pre>";print_r($data);die;
       echo json_encode($data);
    }

    
    private function checkAccess()
      {  
         if($this->aauth->is_loggedin())
         {
            return  $this->session->all_userdata();
         }
         else
         {
            redirect('/auth/login/'); 
            die();
         }
      }
    
    
}
