<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Purchase_return_model extends CI_Model
{   
  
  public function __construct()
  {
   $this->load->database();
   
 }
  /**
  * Purchase_return_model:: get_vendors
  * @return array
  */
  public function get_vendors()
  {
    $this->db->select('vendor_id,vendor_name');
    $this->db->order_by('vendor_name', 'ASC');
    $query = $this->db->get('vendors');
    if($query->num_rows() > 0){
      return $query->result_array();
    }
    else{
    return false;
  } 
  }
   /**
  * Purchase_return_model:: get_marketplaces
  * @return array
  */
  public function get_marketplaces()
  {
  $this->db->select('marketplace_id,marketplace_name');
  $this->db->order_by('marketplace_name', 'ASC');
    $query = $this->db->get('marketplaces');
    if($query->num_rows() > 0){
      return $query->result_array();
    }
    else{
    return false;
  } 
  }
   /**
  * Purchase_return_model:: get_categories
  * @return array
  */
   public function get_categories()
   {
    $this->db->select('category_id,category_name');
    $this->db->order_by('category_name', 'ASC');
    $query = $this->db->get('categories');
    if($query->num_rows() > 0){
      return $query->result_array();
    }
    else{
    return false;
  } 
  }
    /**
  * Purchase_return_model:: save_purchase_return
  * @return array
  */

  public function save_purchase_return(){
    $user_id=$this->aauth->get_user_id();
    $product_items = $this->input->post('product_items')?:array();
    foreach($product_items as $key=>$product_item){
      $this->db->select('stock,item_title')
      ->from('products')
      ->where('product_id',$product_item['product_id']);

      $stock = $this->db->get()->result_array();

      if(!$stock[0]['stock'] > 0 || $stock[0]['stock'] < $product_item['item_qty']){
        return $stock;
      }
    }

     $data = array(
      "vendor_id"=>$this->input->post("vendor_name"),
      "marketplace_id"=>$this->input->post("marketplace_name"),
      "purchase_date"=>date("Y-m-d", strtotime($this->input->post("date_purchased"))),
      "total_price"=>$this->input->post("grand_total"),
      "is_returned"=>1,
      "created_by"=>  $user_id,
      "created" => date('Y-m-d H:i:s',time()),
    );
     $this->db->trans_start();
        if($this->db->insert('purchases',$data))
        {
            $purchase_id  = $this->db->insert_id();
            $items  = array();
            
            foreach($product_items as $i => $product_item)
            {   
                $itemData["item_title"] = $product_item['item_title'];
                $itemData["category_id"] = $product_item['category_id'];
                $itemData["sku"] = $product_item['sku'];
                $itemData["location"] = $product_item['location'];
                $itemData["conditions"] = $product_item['condition'];
                $itemData["date_listed"] = date("Y-m-d", strtotime($this->input->post("date_purchased")));
                $itemData["purchase_id"] = $purchase_id ;
                $itemData["item_qty"] = $product_item['item_qty'];
                $itemData["unit_cost"] = $product_item['unit_cost'];
                $itemData["total_cost"] = $product_item['total_cost'];
                $itemData["product_id"] = $product_item['product_id'];
                $itemData["estimate_price_per_unit"] = $product_item['estimate_price_per_unit'];
                array_push($items,$itemData);
            }
            if(count($items)>0)
            {
               if($this->db->insert_batch('purchase_items',$items))
               {
                //removing duplicate within post array by adding qty
               $forProducts =array(); 
               foreach($items as $key => $val)
               {
                 if(!isset($forProducts[$val["product_id"]]))
                  $forProducts[$val["product_id"]] = $val;
                  else
                  {
                    $forProducts[$val["product_id"]]["item_qty"] = $forProducts[$val["product_id"]]["item_qty"] + $val["item_qty"];
                  } 
               }
               $currentProducts = $this->get_products();
               $allUpdate = array();
               foreach($forProducts as $key => $val)
               {
                 if(isset($currentProducts[$key]))
                 {
                    $dataForUpdate["stock"] = $currentProducts[$key]-$val["item_qty"]; 
                    $dataForUpdate["product_id"] = $val["product_id"];
                    $dataForUpdate["item_title"] = $val["item_title"];
                    $dataForUpdate["location"] = $val["location"];
                    $dataForUpdate["conditions"] = $val['conditions'];
                    $dataForUpdate["category_id"] = $val["category_id"];
                    $dataForUpdate["unit_cost"] = $val["unit_cost"];  
                    array_push($allUpdate,$dataForUpdate);
                 }
               } 
               if(count($allUpdate)>0)
               $this->db->update_batch('products',$allUpdate,'product_id');
                
                $this->db->trans_complete();
                return count($items);
               }
               return false;
            }
           return false; 
        }
        return false;
  }

  /**
  * Purchase_return_model:: get_products
  * @return array
  */

     public function get_products(){
       
        $this->db->where('is_deleted',0);
        $q = $this->db->get('products');
        if($q->num_rows()>0)
        {
            $rows = $q->result_array();
            foreach($rows as $val)
            {
                $rdata[$val["product_id"]] = $val["stock"];
            }
            return $rdata;
        }
        return false;
    }

     /**
  * Purchase_return_model:: get_purchase
  * @param purchase_id
  * @return array
  */

     public function get_purchase($purchase_id)
     {  
        $this->db->select('purchases.*,vendors.vendor_name,marketplaces.marketplace_name');
        $this->db->join('vendors','purchases.vendor_id = vendors.vendor_id','left');
        $this->db->join('marketplaces','purchases.marketplace_id = marketplaces.marketplace_id','left');
        $this->db->where('purchase_id',$purchase_id);
        $query = $this->db->get('purchases');

        if($query->num_rows()>0)
        {
            $data["purchase_details"] = $query->result_array();
            
            $this->db->select('purchase_items.*,categories.category_name');
            $this->db->join('categories','purchase_items.category_id = categories.category_id','left');
            $this->db->where('purchase_id',$purchase_id);
            $query = $this->db->get('purchase_items');
            if($query->num_rows()>0)
            {
               $data["purchase_items"] =$query->result_array();
            }
            else{
              $data["purchase_items"] = array();
            }
            return $data;
        }
        return false;
    }

      /**
  * Purchase_return_model:: get_purchase_details
  * @param purchase_id
  * @return array
  */

    public function get_purchase_details($purchase_id)
    {
        $this->db->select('purchases.*');
        $this->db->where('purchases.purchase_id',$purchase_id);
        $query = $this->db->get('purchases');
        if($query->num_rows() > 0)
        return $query->result_array();
        return false;
    }

    /**
  * Purchase_return_model:: update_purchase_return
  * @param purchase_id
  * @return array
  */
 
 public function update_purchase_return($purchase_id)
    {
     $data = array(
      "vendor_id"=>$this->input->post("vendor_name"),
      "marketplace_id"=>$this->input->post("marketplace_name"),
      "purchase_date"=>date("Y-m-d", strtotime($this->input->post("date_purchased"))),
      "total_price"=>$this->input->post("grand_total"),
     );
      $this->db->where('purchase_id',$purchase_id);
      if($this->db->update('purchases',$data))
      return true;
      
      return false;
    }

      
    /**
  * Purchase_return_model:: get_purchase_item
  * @param purchase_item_id
  * @return array
  */
    

   public function get_purchase_item($purchase_item_id)
    {
        $this->db->select('*');
        $this->db->where('purchase_item_id',$purchase_item_id);
        $query = $this->db->get('purchase_items');
        if($query->num_rows() > 0)
        return $query->result_array();
        
        return false;
    }

      /**
  * Purchase_return_model:: update_purchase_return_item
  * @param purchase_item_id
  * @return array
  */

    public function update_purchase_return_item($purchase_item_id)
    {
        $this->db->where('purchase_item_id',$purchase_item_id);
        $q = $this->db->get('purchase_items');
        $oldRecord = $q->row_array();
        $this->db->where('sku',$oldRecord['sku']);
         $query = $this->db->get('products');
         $product=$query->row_array();
        $data["category_id"] = $this->input->post('category_name');
        $data["item_title"] = $this->input->post('item_title');
        $data["item_qty"] = $this->input->post('item_qty');
        $data["unit_cost"] = $this->input->post('unit_cost');
        $data["total_cost"] = $this->input->post('total_cost');
        $data["location"] = $this->input->post('location');
        $data["date_listed"] = date("Y-m-d", strtotime($this->input->post('date_listed')));
        $data["conditions"] = $this->input->post('condition');
        $data["estimate_price_per_unit"] = $this->input->post('estimate_price_per_unit');

        if($data["item_qty"]>($product['stock']+$oldRecord["item_qty"])){
          $diff=$data["item_qty"]-($product['stock']+$oldRecord["item_qty"]);
          $result=array();
          $result['stock']=$product['stock'];
          $result['diffrance']=$diff;
          $result['item_title']=$product['item_title'];
          return $result;
        }
        else{
        $this->db->where('purchase_item_id',$purchase_item_id);
        if($this->db->update('purchase_items',$data)){
                //updating related product qty or mrp if needed.
                 $product_data = array();
                 if($oldRecord["unit_cost"]!=$data["unit_cost"])
                 $product_data["unit_cost"] = $data["unit_cost"];
                 
                 if($oldRecord["item_qty"] > $data["item_qty"]){ //decreased
                   $diff =  $oldRecord["item_qty"]- $data["item_qty"];
                   $product_data["stock"] = $product["stock"] +$diff;
                 }
                 
                 if($oldRecord["item_qty"] < $data["item_qty"]){ //increased
                   $diff =  $data["item_qty"] - $oldRecord["item_qty"];
                   $product_data["stock"] = $product["stock"]- $diff;
                 }
            if(count($product_data)>0){
            $this->db->where('sku',$oldRecord['sku']);
            $this->db->update('products',$product_data);
            } 
         return true;   
        }
        
        return false;
        }
    }
    
        /**
  * Purchase_return_model:: delete_purchase_return_item
  * @param item_id
  * @return array
  */

   public function delete_purchase_return_item($item_id){
        $this->db->where('purchase_item_id',$item_id);
        $query = $this->db->get('purchase_items');
        if($query->num_rows()==1)
        {
             $itemDetails = $query->result_array();
             $this->db->where('sku',$itemDetails[0]["sku"]);
             $q = $this->db->get('products');
             if($q->num_rows()==1)
             {
                 $productDetails = $q->result_array();
                 $updateData["stock"] = $productDetails[0]["stock"] - $itemDetails[0]["item_qty"];
                 
                 $this->db->where('products.product_id', $productDetails[0]["product_id"]);
                 if($this->db->update('products',$updateData))
                 {
                    $this->db->where('purchase_item_id',$item_id);
                    if($this->db->delete('purchase_items'))
                    return $itemDetails[0]["purchase_id"];
                 }
                 return false;
             }
             return false;
        }
         return false;
    }

    /**
  * Purchase_return_model:: search_product
  * @param title
  * @return array
  */


 function search_product($title){
   $this->db->like('item_title', $title , 'both');
   $this->db->where('is_deleted',0);
   $this->db->order_by('item_title', 'ASC');
   $this->db->limit(10);
   return $this->db->get('products')->result();
 }

    /**
  * Purchase_return_model:: get_products_details
  * @param product_id
  * @return array
  */

 function get_products_details($product_id=false){
  $this->db->select('*');
  $this->db->where('is_deleted',0);
   $this->db->where('product_id',$product_id);
  $q = $this->db->get('products');
  if($q->num_rows()>0)
  {
     return $q->result_array();
  }
  return false;
}
}