<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Sales_model extends CI_Model
{

  public function __construct()
  {
    $this->load->database();

  }

  /**
  * Sales_model:: save_sale
  * @return array
  */

   public function save_sale(){

    $company_id=$this->session->userdata('company_id');
    $user_id=$this->aauth->get_user_id();
       $data = array(
        "company_id"=>$company_id,
        "invoice_number"=>$this->input->post("invoice_number"),
        "invoice_date"=>date("Y-m-d", strtotime($this->input->post("invoice_date"))),
        "wherehouse_id"=>$this->input->post("wherehouse"),
        "client_id"=>$this->input->post("client_name"),
        "client_name"=>$this->input->post("client_orgnisation_name"),
        "client_address"=>$this->input->post("client_address_b"),
        "client_city"=>$this->input->post("client_city_b"),
        "client_state"=>$this->input->post("client_state_b"),
        "client_country"=>$this->input->post("client_country_b"),
        "client_zip"=>$this->input->post("client_zip_b"),
        "client_phone"=>$this->input->post("client_mobile"),
        "client_email"=>$this->input->post("client_email"),
        "client_tax_no"=>$this->input->post("client_tax_number"),
        "b_address"=>$this->input->post("client_address_b"),
        "b_city"=>$this->input->post("client_city_b"),
        "b_state"=>$this->input->post("client_state_b"),
        "b_country"=>$this->input->post("client_country_b"),
        "b_zip"=>$this->input->post("client_zip_b"),
        "s_address"=>$this->input->post("client_address_s"),
        "s_city"=>$this->input->post("client_city_s"),
        "s_state"=>$this->input->post("client_state_s"),
        "s_country"=>$this->input->post("client_country_s"),
        "s_zip"=>$this->input->post("client_zip_s"),
        "transportation_mode"=>$this->input->post("transportation_mode"),
        "distance_to_transport"=>$this->input->post("distacnce_to_transport"),
        "transporter_name"=>$this->input->post("transporter_name"),
        "transporter_id"=>$this->input->post("transporter_id"),
        "transporter_document_no"=>$this->input->post("transporter_document_no"),
        "transporter_document_date"=>date("Y-m-d", strtotime($this->input->post("transporter_document_date"))),
        "vechicle_no"=>$this->input->post("vechicle_no"),
        "place_of_supply"=>$this->input->post("place_of_supply"),

        "client_order"=>$this->input->post("buyer_order"),
        "dispatch_document_no"=>$this->input->post("dispatch_document_no"),
        "delivery_note_date"=>$this->input->post("delevery_note_date"),
        "dispatch_through"=>$this->input->post("dispatch_through"),
        
        "is_reverce_charge"=>$this->input->post("reverse_charge")?$this->input->post("reverse_charge"):0,
        "is_paid"=>$this->input->post("ispaid")?$this->input->post("ispaid"):0,
        "notes"=>$this->input->post("notes"),

        "sub_total"=>$this->input->post("sub_total_amount"),
        "total_discount"=>$this->input->post("total_discount_amount"),
        "total_tax"=>$this->input->post("total_tax_amount"),
        "grand_total"=>$this->input->post("grand_total"),
        "created_by"=>  $user_id,
        "created" => date('Y-m-d H:i:s',time()),
      );
     $this->db->trans_start();
        if($this->db->insert('sales',$data))
        {
            $sale_id  = $this->db->insert_id();
            $items  = array();
            $product_items = $this->input->post('item')?:array();
            foreach($product_items as $i => $product_item)
            {   
                            
                $taxdata=$this->get_tax($product_item['tax']);
                $itemData["sale_id"]= $sale_id;
                $itemData["product_id"] = $product_item['product_id']?$product_item['product_id']:'0';
                $itemData["item_title"] = $product_item['item_title'];
                $itemData["measurement_id"] = $product_item['measurement_id'];
                $itemData["measurement_name"] = $product_item['measurement_name'];
                $itemData["hsn_sac_code"] = $product_item['hsn_sac_code'];
                $itemData["item_qty"] = $product_item['item_qty'];
                $itemData["unit_cost"] = $product_item['unit_cost'];
                $itemData["total_cost"] = $product_item['total_amount'];
                $itemData["discount_id"] = $product_item['discount'];
                $itemData["discount"] = $product_item['discount_name'];
                $itemData["discount_amount"] = $product_item['discount_amount'];
                $itemData["tax_id"] = $product_item['tax'];
                $itemData["tax"] = $product_item['tax_name'];
                $itemData["is_apply_group_tax"] = $taxdata['is_grouped'];
                $itemData["taxes_groups"] = $taxdata['taxe_groups'];
                $itemData["tax_amount"] = $product_item['tax_amount'];
                $itemData["grand_total_coast"] = $product_item['total_gamount'];
                $itemData["created"] = date('Y-m-d H:i:s',time());
                array_push($items,$itemData);
            }
            if(count($items)>0)
            {
               if($this->db->insert_batch('sale_items',$items))
               {
                //removing duplicate within post array by adding qty
               $forProductsStock =array(); 
               foreach($items as $key => $val)
               {
                 if(!isset($forProductsStock[$val["product_id"]]))
                 {
                  $forProductsStock[$val["product_id"]] = $val;
                 }
                  else
                  {
                    $forProductsStock[$val["product_id"]]["item_qty"] = $forProductsStock[$val["product_id"]]["item_qty"] + $val["item_qty"];
                  } 
               }
               $currentstock = $this->get_stock($this->input->post("wherehouse"));
               $allInserts = array();
               $allUpdate = array();
                foreach($forProductsStock as $key => $val)
                 {
                    if($currentstock[$key]['stock']>0){
                    $dataForUpdate["product_id"] = $val["product_id"];
                    $dataForUpdate["stock"] = $currentstock[$key]['stock']-$val["item_qty"]; 
                    array_push($allUpdate,$dataForUpdate);
                    }
                 }
               if(count($allUpdate)>0)
               $this->db->update_batch('stocks',$allUpdate,'product_id');
               $this->db->trans_complete();
                return count($items);
               }
               return false;
            }
           return false; 
        }
        return false;
  }

        public function get_sale($sale_id)
       {  
        $company_id=$this->session->userdata('company_id');
        $this->db->select('sales.*');
        $this->db->where('sale_id',$sale_id);
        $this->db->where('sales.company_id',$company_id);
        $query = $this->db->get('sales');

        if($query->num_rows()>0)
        {
            $data["sale_details"] = $query->result_array();
            
            $this->db->select('sale_items.*');
            $this->db->where('sale_id',$sale_id);
            $query = $this->db->get('sale_items');
            if($query->num_rows()>0)
            {

               $data["sale_items"] =$query->result_array();
            }
            else{
              $data["sale_items"] = array();
            }
            return $data;
        }
        return false;
    }

   public function get_stock($wherehouse_id){
       $company_id=$this->session->userdata('company_id'); 
       $this->db->select('*');
       $this->db->where('company_id',$company_id);
       $this->db->where('wherehouse_id',$wherehouse_id);
        $q = $this->db->get('stocks');
        if($q->num_rows()>0)
        {
            $rows = $q->result_array();
            foreach($rows as $val)
            {
                $rdata[$val["product_id"]] = $val;
            }
            return $rdata;
        }
        return false;
    }
    public function get_tax($id){
      $company_id=$this->session->userdata('company_id'); 
       $this->db->select('*');
       $this->db->where('company_id',$company_id);
       $this->db->where('tax_id',$id);
        $q = $this->db->get('taxes');
        if($q->num_rows()>0)
        {
            $rows = $q->row_array();
            return $rows;
        }
        return false;
    }
 public function  get_client_details($id){
     $company_id=$this->session->userdata('company_id');
    $this->db->select('*');
    $this->db->where('client_id',$id);
    $this->db->where('company_id',$company_id);
    $query = $this->db->get('clients');
    if($query->num_rows() > 0)
    return $query->row_array();
        
    return false;
   }
 }

