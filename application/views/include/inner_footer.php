 <footer class="main-footer">
        <div class="pull-right hidden-xs">
         
        </div>
        <strong>Copyright &copy; <?php echo date("Y");?> <a class="text-custom" href="<?php echo site_url(); ?>"><?php echo $this->config->item('name', 'aauth'); ?></a>.</strong> All rights reserved.
     
     
      </footer>
      </div>
      <script type="text/javascript">
        $(document).ajaxStart(function() { Pace.restart(); });
      </script>
      </body>
       <!-- jQuery UI 1.11.4 -->
    
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/app.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/custom.js');?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url('assets/js/jquery.slimscroll.min.js');?>"></script>
  </html>
       