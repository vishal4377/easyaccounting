<?php
if($details){
?>
<div class="row">
 <div class="col-md-5">
  <table class="table table-striped table-bordered table-condensed" id="table" style="width: 100%;">
        <tr>
            <td><strong>SKU Id:</strong></td>
            <td><?php  echo htmlspecialchars($details[0]["sku"]); ?></td>
        </tr>
        <tr>
            <td><strong>Product Name:</strong></td>
            <td><?php echo htmlspecialchars($details[0]["item_title"]); ?></td>
        </tr>
        <tr>
            <td><strong>HSN/SAC Code:</strong></td>
            <td><?php echo htmlspecialchars($details[0]["hsn_sac_code"]); ?></td>
        </tr>
         <tr>
            <td><strong>Category:</strong></td>
            <td><?php  echo htmlspecialchars($details[0]["category_name"]); ?></td>
        </tr>
        <tr>
            <td><strong>Brand:</strong></td>
            <td><?php  echo htmlspecialchars($details[0]["brand_name"]); ?></td>
        </tr>
        <tr>
            <td><strong>Tax:</strong></td>
            <td><?php  echo htmlspecialchars($details[0]["tax_name"].''.$details[0]["tax_percentage"].'%'); ?></td>
        </tr>
        <tr>
            <td><strong>Measurement:</strong></td>
            <td><?php  echo htmlspecialchars($details[0]["measurement_name"]); ?></td>
        </tr>
         <tr>
            <td><strong>Product Price:</strong></td>
            <td><?php  echo htmlspecialchars($details[0]["unit_cost"]); ?></td>
        </tr>
         <tr>
            <td><strong>Salling price:</strong></td>
            <td><?php  echo htmlspecialchars($details[0]["salling_price"]); ?></td>
        </tr>
        <tr>
            <td><strong>Product Description:</strong></td>
            <td><?php  echo htmlspecialchars($details[0]["product_description"]); ?></td>
        </tr>
         <tr>
            <td><strong>Created:</strong></td>
            <td><?php echo date("m/d/Y", strtotime($details[0]["created"])); ?></td>
        </tr>
  </table>
  </div>  
  <div class="col-md-7">
  <h4>History</h4>
  
  
  <?php
  if(isset($hrecords)){
  ?>
  <table id="history_tbl" class="table table-bordered table-condensed">
        <thead>
        <tr>
            <th>Date</th>
            <th>Activity</th>
            <th>Vendor</th>
            <th>Purchased</th>
            <th>Listed</th>
            <th>Stock</th>
        </tr>
        </thead>
 <tbody>
  <?php  
    foreach($hrecords as $record){
  ?>  
    <tr>
    <td><?php if($record["created"]!='0000-00-00') echo date("m/d/Y", strtotime($record["created"])); else echo '-'; ?></td>
    <td><?php echo $record["activity"]; ?></td>
    <td><?php echo $record["vendor_name"]; ?></td>
    <td><?php if($record["date_purchased"]!='0000-00-00') echo date("m/d/Y", strtotime($record["date_purchased"])); else echo '-'; ?></td>
    <td><?php if($record["date_unlisted"]!='0000-00-00') echo date("m/d/Y", strtotime($record["date_unlisted"])); else echo '-'; ?></td>
    <td><?php echo $record["stock"]; ?></td>
    </tr>
  <?php  
    }
  ?>
  </tbody>
  </table>
  <?php    
  }
  else{
    echo '<div class="alert alert-info">No records found.</div>';
  }
  
  ?>
  
  </div>    
  </div>
<?php    
}