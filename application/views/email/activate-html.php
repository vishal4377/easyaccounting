<?php 
$details['title']="Account Activate";
$this->load->view('email/email_header',$details);?>
                                         
                                            <div class="cContent" id="cContent-4" style="padding: 15px;background-color: rgb(53, 53, 53);;">
                                                <table width="100%" cellspacing="0" cellpadding="0" style="padding: 0px; padding-top: 0px; padding-bottom: 15px;">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0" style="">
                                                                    <tbody>
                                                                        <tr>
                                                                        	
                                                                            <td bgcolor="">
                                                                                <div id="txtHolder-4" class="txtEditorClass" style="color: #ffffff;;font-size: 26px;font-family: 'Impact';text-align: Left;">Welcome to <?php echo $name; ?>!</div>
                                                                            </td>
                                                                            <td width="1">
                                                                                <a href="http://" target="_blank">
                                                                                    <div class="ui-wrapper" style="overflow: hidden;position: relative;width: 100%;"><img src="<?php echo base_url('assets/images/shards_logom.png')?>" class="txtImg editableImg ui-resizable" id="img-2" width="100" border="0" alt="" title="" style="margin: 0px;resize: none;position: static;zoom: 1;display: block;width: 85px;">
                                                                                    </div>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="cContent" id="cContent-5" style="padding: 15px;background-color: rgba(255, 255, 255,0.9);">
                                                <table width="100%" cellspacing="0" cellpadding="0" style="padding: 0px; padding-top: 0px; padding-bottom: 15px;">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellpadding="0" cellspacing="0" style="">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td bgcolor="">
                                                                                <div id="txtHolder-5" class="txtEditorClass" style="color: #666666; font-size: 14px; font-family: 'Arial'; text-align: Left">
                                                                                  Thanks for joining <?php echo $name; ?>. We listed your sign in details below, make sure you keep them safe.<br />
                                                                                  To verify your email address, please follow this link:
                                                                                  <br>
                                                                                  <big style="font: 16px/18px Arial, Helvetica, sans-serif;"><b><a href="<?php echo site_url('/auth/email_verification/'.$user_id.'/'.$verification_code); ?>" style="color: #3366cc;">Finish your registration...</a></b></big><br />
                                                                                  <br />
                                                                                  Link doesn't work? Copy the following link to your browser address bar:<br />
                                                                                  <nobr><a href="<?php echo site_url('/auth/email_verification/'.$user_id.'/'.$verification_code); ?>" style="color: #3366cc;"><?php echo site_url('/auth/email_verification/'.$user_id.'/'.$verification_code); ?></a></nobr><br />
                                                                                  <br>
                                                                                  <br>
                                                                                  <br />
                                                                                  Have fun!<br />
                                                                                  The <?php echo $name; ?> Team
                                                                              </div>
                                                                          </td>
                                                                      </tr>
                                                                  </tbody>
                                                              </table>
                                                          </td>
                                                      </tr>
                                                  </tbody>
                                              </table>
                                          </div>
 <?php $this->load->view('email/email_footer');?>
