<?php
if($details)
{
  $purchase_details = $details['purchase_details'][0];
  $purchase_items = $details['purchase_items'];
  ?>
<div class="content-wrapper" style="min-height: 916px;">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 class="pull-left">
     <?php echo @$title; ?>
     <small></small>
   </h1>
  
   <div class="pull-right" >
    <a href="<?php echo site_url('purchases');?>" class="btn btn-default" title="Back"><span class="glyphicon glyphicon-arrow-left" style="margin-right: 5px;"></span>Back</a>
  </div> 
  <div class="clearfix"></div>
</section>

<!-- Main content -->
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> <?php echo $company_profile['company_name'];?>
            <small class="pull-right">Date: <?php echo date("m/d/Y", strtotime($purchase_details['purchase_date']));?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-3 invoice-col">
          From
          <address>
            <strong><?php echo $purchase_details['supplier_name'];?></strong><br>
            <?php echo $purchase_details['supplier_address'];?><br>
            <?php echo $purchase_details['supplier_city'].', '.$purchase_details['supplier_state'];?><br>
            <?php echo $purchase_details['supplier_country'].', '.$purchase_details['supplier_zip'];?><br>
            Phone: <?php echo $purchase_details['supplier_phone'];?><br>
            Email: <?php echo $purchase_details['supplier_email'];?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-3 invoice-col">
          Bill To
          <address>
            <strong><?php echo $company_profile['company_name'];?></strong><br>
            <?php echo $purchase_details['b_address'];?><br>
            <?php echo $purchase_details['b_city'].', '.$purchase_details['b_state'];?><br>
            <?php echo $purchase_details['b_country'].', '.$purchase_details['b_zip'];?><br>
            Phone: <?php echo $company_profile['mobile'];?><br>
            Email: <?php echo $company_profile['email'];?>
          </address>
        </div>
         <!-- /.col -->
        <div class="col-sm-3 invoice-col">
          Ship To
          <address>
            <strong><?php echo $company_profile['company_name'];?></strong><br>
            <?php echo $purchase_details['s_address'];?><br>
            <?php echo $purchase_details['s_city'].', '.$purchase_details['s_state'];?><br>
            <?php echo $purchase_details['s_country'].', '.$purchase_details['s_zip'];?><br>
            Phone: <?php echo $company_profile['mobile'];?><br>
            Email: <?php echo $company_profile['email'];?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-3 invoice-col">
          <b>Invoice <?php echo $purchase_details['supplier_invoice'];?></b><br>
          <br>
          <b>Order ID:</b> <?php echo $purchase_details['supplier_order'];?><br>
          <b>Dispatch Document No:</b> <?php echo $purchase_details['dispatch_document_no'];?><br>
          <b>Delivery Note Date:</b> <?php echo date("m/d/Y", strtotime($purchase_details['delivery_note_date']));?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-bordered table-striped table-condensed">
            <thead>
            <tr>
              <th>Serial</th>
              <th>Product</th>
              <th>Unit</th>
              <th>Price</th>
              <th>Qty</th>
              <th>Subtotal</th>
              <th colspan="2">Discount</th>
              <th colspan="2">Tax</th>
              <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <?php 
            $i=1;
            foreach ($purchase_items as $purchase_item) {
             ?>
            <tr>
              <td><?php echo $i;?></td>
              <td><?php echo $purchase_item['item_title'];?></td>
              <td><?php echo $purchase_item['measurement_name'];?></td>
              <td><?php echo $purchase_item['unit_cost'];?></td>
              <td><?php echo $purchase_item['item_qty'];?></td>
              <td><?php echo $purchase_item['total_cost'];?></td>
              <td><?php echo $purchase_item['discount'];?></td>
              <td><?php echo $purchase_item['discount_amount'];?></td>
              <td><?php echo $purchase_item['tax'];?></td>
              <td><?php echo $purchase_item['tax_amount'];?></td>
              <td><?php echo $purchase_item['grand_total_coast'];?></td>
            </tr>
            <?php $i++; }?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <?php if($purchase_details['notes']){?>
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           <?php echo $purchase_details['notes'];?>
          </p>
        <?php }?>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <div class="table-responsive">
            <table class="table">
              <tbody><tr>
                <th style="width:50%">Subtotal:</th>
                <td><?php echo $purchase_details['sub_total'];?></td>
              </tr>
              <tr>
                <th>Total Tax:</th>
                <td><?php echo $purchase_details['total_discount'];?></td>
              </tr>
              <tr>
                <th>Total Discount:</th>
                <td><?php echo $purchase_details['total_tax'];?></td>
              </tr>
              <tr>
                <th>Total:</th>
                <td><?php echo $purchase_details['grand_total'];?></td>
              </tr>
            </tbody></table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- this row will not appear when printing -->
    <!--   <div class="row no-print">
        <div class="col-xs-12">
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div> -->
    </section>
    <div class="clearfix"></div>
</div>
 <?php
}else{
  echo "No Record Found.";
}
?>
<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js');?>"></script>


